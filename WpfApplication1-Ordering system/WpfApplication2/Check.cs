﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace WpfApplication2
{
    class Check
    {
        // 動作完成時間門檻 0.5 秒
        TimeSpan DurationThreshold = new TimeSpan(5000000);
        const double DistThreshold = 0.30;  // 移動距離門檻 (0.30 米)
        const double ZposThreshold = 0.10;  // Z座標差距門檻 (0.10 米)
        // 偵測 Next 的起始時間
        TimeSpan initialTimestamp = new TimeSpan(-1);
        double startPosition = -1;          // 偵測開始時左手的 X 座標
        double previousPosition = -1;       // 前一骨架左手的 X 座標
        private int state = 0;              // 0:Failure 1:Progress


        // 偵測 previous 姿勢後觸發事件 Detected
        public event EventHandler Detected;
        // 偵測 previous 姿勢的函式為 Detection
        public void Detection(Joint rightShoulder, Joint rightWrist, Joint rightElbow,
            TimeSpan currentTimestamp)
        {
            if (state == 0)   // Failure
            {   
                if (rightWrist.Position.X > rightElbow.Position.X && ( Math.Abs(rightShoulder.Position.Y-rightWrist.Position.Y) < 0.1 ) )//&& (rightElbow.Position.Y - rightWrist.Position.Y <= 0.1)
                {
                    initialTimestamp = currentTimestamp;  //存時間
                    startPosition = rightWrist.Position.Y; //存動作
                    previousPosition = rightWrist.Position.Y;
                    state = 1;
                }
            }
            else    // Progress
            {
                if (rightWrist.Position.Y < previousPosition)
                {
                    if (startPosition - rightWrist.Position.Y >
                        DistThreshold)  //DistThreshold 為30CM
                    {
                        Detected(this, new EventArgs());
                        TimeSpan initialTimestamp = new TimeSpan(-1);
                        startPosition = -1;
                        previousPosition = -1;
                        state = 0;
                    }
                    else
                    {
                        if (currentTimestamp - initialTimestamp >
                             DurationThreshold)
                        {
                            initialTimestamp = new TimeSpan(-1);
                            startPosition = -1;
                            previousPosition = -1;
                            state = 0;
                        }
                        else
                            previousPosition = rightWrist.Position.Y;
                    }
                }
                else
                {
                    TimeSpan initialTimestamp = new TimeSpan(-1);
                    startPosition = -1;
                    previousPosition = -1;
                    state = 0;
                }
            }
        }
    }
}
