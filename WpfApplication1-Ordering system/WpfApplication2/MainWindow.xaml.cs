﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;     // 連結 Kinect One (Kinect SDK 2.0)


namespace WpfApplication2
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        // sensor 代表接到電腦的那一台 Kinect One (只能接一台)
        private KinectSensor sensor;
        private ColorFrameReader colorFrameReader;
        private FrameDescription frameDescription;
        private WriteableBitmap wbData; // 儲存影像的記憶體區塊
        private byte[] byteData;  // 儲存影像每一 pixel 之 RGB 值的陣列參考
        private int color;        // 1：紅色影像  2：綠色影像  3：藍色影像
        private int count = 0;    // 已儲存影像數目

        private BodyFrameReader bodyFrameReader;
        private Body[] bodies;  // 骨架陣列，陣列每一個元素代表一個人的骨架

        Next nextdetec = new Next(); //左手
        Previous previous = new Previous();   //右手
        Check check = new Check();
        Calculate cal = new Calculate();
        //  Up up = new Up();


        private string[] file = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\MImage");

        private string fileName; //儲存一個影像檔案的完整檔名
        private int i = 1;//第一個影像開始
        private int menunum;//菜單餐點數目
        //字串str顯示照片中的餐點名稱
        string[] str = new string[] { "吮指雙雞套餐", "美式BBQ醬烤煙燻雞餐", "韓國甜辛紙包雞XL餐", "美國BBQ醬烤煙燻雞XL餐", "吮指雙雞重量級XL餐", "咔啦雞腿堡重量級XL套餐", "紐奧良烙烤雞腿堡重量級XL套餐", "惡魔醬淋咔啦雞腿堡重量級XL套餐" };
        string[] price = new string[] { "129", "129", "165", "165", "165", "160", "160", "165" };

        int subtotal = 0;
        int total = 0;

        public MainWindow()
        {
            InitializeComponent();

            //---------------------------
            
            sensor = KinectSensor.GetDefault();
            colorFrameReader = sensor.ColorFrameSource.OpenReader();
            colorFrameReader.FrameArrived += ColorFrameReader_FrameArrived;
            frameDescription =
                sensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
            // wbData參考一個 WriteableBitmap 物件(儲存影像的記憶體區塊)
            wbData = new WriteableBitmap(
                frameDescription.Width, frameDescription.Height, 96, 96,
                PixelFormats.Bgr32, null);
            // byteData 參考一個儲存影像每一個 pixel 之 RGB 值的 byte 陣列
            byteData = new byte[frameDescription.Width * frameDescription.Height * 4];

            bodyFrameReader = sensor.BodyFrameSource.OpenReader();
            bodyFrameReader.FrameArrived += BodyFrameReader_FrameArrived;

            // nextdetec 觸發的 Detected 事件由 nextdetec_Detected() 處理
            nextdetec.Detected += Nextdetec_Detected;
            // previous 觸發的 Detected 事件由 Previous_Detected() 處理
            previous.Detected += Previous_Detected;

            check.Detected += Check_Detected;
            //-----------  up.Detected += Up_Detected;

            cal.Detected += Cal_Detected;

            // 啟動 Kinect Sensor
            sensor.Open();
            
            //---------------------------

            menunum = file.Length;//取得menu影像檔案個數
            label7.Content = subtotal;

        }

        private void Cal_Detected(object sender, EventArgs e)
        {
            //計算總金額

            if (textBox.Text!="")
            {
                MessageBox.Show("謝謝惠顧，您總共花了" + total + "元");
                textBox.Text = "";
                label7.Content = "";
                label8.Content = "";
                total = 0;
                subtotal = 0;
            }
           
        }

        private void Check_Detected(object sender, EventArgs e)
        {
            if (i <= file.Length && i >= 1)
            {
                textBox.Text = textBox.Text + str[i - 1] + "\r\n";
                subtotal += Convert.ToInt16(price[i - 1]);
                label7.Content = subtotal;
                total += Convert.ToInt16(price[i - 1]);
            }
            else
            {
                textBox.Text += null;
            }
        }

        private void Previous_Detected(object sender, EventArgs e)
        {
            button.Visibility = Visibility.Visible;
            if (i > 1)
            {
                i--; //前一張圖

            }
            else
            {
                i = file.Length;

            }
            Order(i);   //呼叫Order的函數  將第幾張的參數傳過去 
            /*
            if (color == 0)     // RED GREEN BLUE COLOR 前一選項
                color = 3;
            else
                color--;

            */
        }

        private void Nextdetec_Detected(object sender, EventArgs e)
        {
            button.Visibility = Visibility.Visible;
            if (i < file.Length)
            {
                i++; //下一張

            }
            else
            {
                i = 1; //第一張

            }
            Order(i);
            /*
            color++;            // RED GREEN BLUE COLOR 後一選項
            if (color > 3)
                color = 0;
            */
        }

        private void ColorFrameReader_FrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            using (ColorFrame colorFrame = e.FrameReference.AcquireFrame())
            {
                // 如果影格資料不存在，就直接離開事件處理函式
                if (colorFrame == null)
                    return;
                using (KinectBuffer kinectBuffer = colorFrame.LockRawImageBuffer())
                {
                    // 將影格每一個 pixel 之 RGB 值儲存至 wbData 物件
                    colorFrame.CopyConvertedFrameDataToIntPtr(wbData.BackBuffer,
                        (uint)(frameDescription.Width * frameDescription.Height * 4),
                        ColorImageFormat.Bgra);
                    Int32Rect bitmapRect = new Int32Rect(0, 0, frameDescription.Width,
                        frameDescription.Height);
                    // 將影格每一個 pixel 之 RGB 值儲存至 byteData 陣列
                    colorFrame.CopyConvertedFrameDataToArray(byteData, ColorImageFormat.Bgra);
                    // 依據 color 的值決定影像紅色、綠色、藍色
                    for (int i = 0; i < byteData.Length; i += 4)
                        switch (color)
                        {
                            case 1: // 紅色
                                byteData[i] = 0x00;
                                byteData[i + 1] = 0x00;
                                break;
                            case 2: // 綠色
                                byteData[i] = 0x00;
                                byteData[i + 2] = 0x00;
                                break;
                            case 3: // 藍色
                                byteData[i + 1] = 0x00;
                                byteData[i + 2] = 0x00;
                                break;
                        }
                    wbData.WritePixels(bitmapRect, byteData, (int)
                        (frameDescription.Width * frameDescription.BytesPerPixel), 0);
                }
            }
        }
        private void BodyFrameReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                // 如果骨架資料不存在就直接離開事件處理常式
                if (bodyFrame == null)
                    return;
                // 產生骨架陣列，陣列長度為 6
                bodies = new Body[bodyFrame.BodyCount];
                // 將骨架資料複製到骨架陣列
                bodyFrame.GetAndRefreshBodyData(bodies);
                for (int i = 0; i < bodies.Length; i++)
                {
                    // 檢查那一個骨架處於被追蹤狀態
                    if (bodies[i].IsTracked)
                    {
                        // 偵測 nextdetec
                        
                        nextdetec.Detection(
                            bodies[i].Joints[JointType.WristLeft],
                            bodies[i].Joints[JointType.ElbowLeft],
                            bodyFrame.RelativeTime);
                        
                        // 偵測 previous
                        
                        previous.Detection(
                            bodies[i].Joints[JointType.WristRight],
                            bodies[i].Joints[JointType.ElbowRight],
                            bodyFrame.RelativeTime);

                        check.Detection(
                            bodies[i].Joints[JointType.ShoulderRight],
                            bodies[i].Joints[JointType.WristRight],
                            bodies[i].Joints[JointType.ElbowRight],
                            bodyFrame.RelativeTime);

                        cal.Detection(
                            bodies[i].Joints[JointType.ShoulderRight],
                            bodies[i].Joints[JointType.WristRight],
                            bodies[i].Joints[JointType.ElbowRight],
                            bodyFrame.RelativeTime);

                    }
                }
            }
        }

      
        private void WpfApplication2_Loaded(object sender, RoutedEventArgs e)
        {
            // 指定 ColorImage 影像來源為 wbData (WriteableBitmap 物件)
            ColorImage.Source = wbData;
        }

        private void WpfApplication2_Unloaded(object sender, RoutedEventArgs e)
        {
                // 取消監聽 ColorFrameReader 之 FrameArrived 事件
                colorFrameReader.FrameArrived -= ColorFrameReader_FrameArrived;
                // 取消監聽 bodyFrameReader 之 FrameArrived 事件
                bodyFrameReader.FrameArrived -= BodyFrameReader_FrameArrived;
                // 取消監聽 nextdetec 之 Detected 事件
               nextdetec.Detected -= Nextdetec_Detected;
                // 停止 Kinect Sensor
                sensor.Close();
        }

        /////////////////////////////////-圖片控制設定-//////////////////////////////////////

        private void Order(int i)
        {

            BitmapImage imgfile = new BitmapImage();
            //啟動bitmapImage物件
            imgfile.BeginInit();
            //儲存完整檔名字的file陣列從0起算，故減1
            fileName = file[i - 1];
            //fileName完整名字含子目錄字元 所以加上@
            imgfile.UriSource = new Uri(@fileName);
            //結束bitmapImage
            imgfile.EndInit();
            //讓圖可以在meuimg控制項上可以顯示
            menuimg.Source = imgfile;
            label1.Content = str[i - 1];
            label3.Content = "NT " + price[i - 1];



        }


        /////////////////////////////////-顯示上一個餐點-//////////////////////////////////////
        private void pre_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            button.Visibility = Visibility.Visible;
            if (i > 1)
            {
                i--; //前一張圖

            }
            else
            {
                i = file.Length;

            }
            Order(i);   //呼叫Order的函數  將第幾張的參數傳過去 

        }

        /////////////////////////////////-顯示下一個餐點-//////////////////////////////////////
        private void next_Click(object sender, RoutedEventArgs e)
        {
            button.Visibility = Visibility.Visible;
            if (i < file.Length)
            {
                i++; //下一張

            }
            else
            {
                i = 1; //第一張

            }
            Order(i);

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            //顯示折價券的輸入框

            discount.Visibility = Visibility.Visible;
            label6.Content = "提示：輸入'count20'即可使用20元折價券唷~";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //add item
            
            if (i <= file.Length && i >= 1)
            {
                textBox.Text = textBox.Text + str[i - 1] + "\r\n";
                subtotal += Convert.ToInt16(price[i - 1]);
                label7.Content = subtotal;
                total += Convert.ToInt16(price[i - 1]);
            }
            else
            {
                textBox.Text += null;
            }


        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //計算總金額

            if (discount.Text != "" && (total != 0 || subtotal != 0))
            {
                if (discount.Text == "count20")
                {//輸入無誤
                    if (subtotal-total<20)
                    {//未使用過

                        total = total - 20;

                        label8.Content = total;
                    }
                    else
                    {//已使用過
                        MessageBox.Show("溫馨提示：折價券限用一次！");
                        label8.Content = total;
                        label7.Content = subtotal;

                    }
                }
                else
                {
                    label8.Content = total;
                    MessageBox.Show("溫馨提示：您的折價券序號輸入有誤~");
                }

            }
            else
            {
                label8.Content = total;
                label7.Content = subtotal;
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            //結帳~
            if (discount.Text != "" && (total != 0 || subtotal != 0))
            {
                if (discount.Text == "count20")
                {  //輸入無誤
                    if (subtotal -total<20)
                    {//尚未用過折價券 
                        total = total - 20;
                        label8.Content = total;
                        MessageBox.Show("謝謝惠顧，您總共花了" + total + "元");

                        textBox.Text = "";
                        label7.Content = "";
                        label8.Content = "";
                        total = 0;
                        subtotal = 0;
                    }
                    else
                    {
                        //已經用過折價券
                      
                        label8.Content = total;
                        label7.Content = subtotal;

                        MessageBox.Show("謝謝惠顧，您總共花了" + total + "元");

                        textBox.Text = "";
                        label7.Content = "";
                        label8.Content = "";
                        total = 0;
                        subtotal = 0;


                    }


                }
                else
                {//輸入錯誤
                    label8.Content = total;
                    MessageBox.Show("溫馨提示：您的折價券序號輸入有誤~");
                }

            }
            else
            {
                label8.Content = total;
                MessageBox.Show("謝謝惠顧，您總共花了" + total + "元");
                textBox.Text = "";
                label7.Content = "";
                label8.Content = "";
                total = 0;
                subtotal = 0;
            }

        }

        
    }
}
