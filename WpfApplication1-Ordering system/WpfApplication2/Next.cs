﻿// Next 類別偵測 "左手往右揮"
// Next 類別偵測到正確姿勢後觸發 Detected 事件
// Next 類別判定姿勢是否正確的函式為 Detection()
// Detection() 利用 WristLeft、ElbowLeft 二個關節座標做判斷

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect; // 連結 Kinect One (Kinect SDK 2.0)
namespace WpfApplication2
{
    class Next
    {
        // 動作完成時間門檻 0.5 秒
        TimeSpan DurationThreshold = new TimeSpan(5000000);
        const double DistThreshold = 0.30;  // 移動距離門檻 (0.30 米)
        const double ZposThreshold = 0.10;  // Z座標差距門檻 (0.10 米)
        // 偵測 Next 的起始時間
        TimeSpan initialTimestamp = new TimeSpan(-1);
        double startPosition = -1;          // 偵測開始時左手的 X 座標
        double previousPosition = -1;       // 前一骨架左手的 X 座標
        private int state = 0;              // 0:Failure 1:Progress


        // 偵測 Next 姿勢後觸發事件 Detected
        public event EventHandler Detected;
        // 偵測 Next 姿勢的函式為 Detection
        public void Detection(Joint leftWrist, Joint leftElbow,
            TimeSpan currentTimestamp)
        {
            if (state == 0)   // Failure
            {
                if (leftWrist.Position.X < leftElbow.Position.X &&
                    (leftElbow.Position.Z - leftWrist.Position.Z >
                    ZposThreshold))
                {
                    initialTimestamp = currentTimestamp;
                    startPosition = leftWrist.Position.X;
                    previousPosition = leftWrist.Position.X;
                    state = 1;
                }
            }
            else    // Progress
            {
                if (leftWrist.Position.X > previousPosition)
                {
                    if (leftWrist.Position.X - startPosition >
                        DistThreshold)
                    {
                        Detected(this, new EventArgs());
                        TimeSpan initialTimestamp = new TimeSpan(-1);
                        startPosition = -1;
                        previousPosition = -1;
                        state = 0;
                    }
                    else
                    {
                        if (currentTimestamp - initialTimestamp >
                             DurationThreshold)
                        {
                            initialTimestamp = new TimeSpan(-1);
                            startPosition = -1;
                            previousPosition = -1;
                            state = 0;
                        }
                        else
                            previousPosition = leftWrist.Position.X;
                    }
                }
                else
                {
                    TimeSpan initialTimestamp = new TimeSpan(-1);
                    startPosition = -1;
                    previousPosition = -1;
                    state = 0;
                }
            }
        }
    }
}
