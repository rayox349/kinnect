﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        bool whichplayer = true; //當whichplayer 為true-> O false-> X
        bool checkAIclick = true; //true表示還沒下 false表示下了
        bool checkwinshow = true; //防止重複跳出視窗
        bool checkwithComputer = false; //確認是否和電腦玩
        //bool PlayerFirst = true;   //預設玩家先手
        bool ComputerFirst = false;//電腦先後手

        int totalclick = 0;//判斷有沒有按滿9次
        int countO = 0, countTie = 0, countX = 0; //countO -> O countTie ->和局 countX -> X


        public MainWindow()
        {
            InitializeComponent();
            //MessageBox.Show("123","456");  //前面是內文 後面是標題
            btnPriority.IsEnabled = false;//預設沒有和電腦玩  不可以按
            endGame();
            btnNewgame.IsEnabled = false;//有下過之後 才可以按重新開始
            labelMode.Content = "請 先 選 擇 模 式";
        }

        private void btn_click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            if (whichplayer)
                btn.Content = "O";
            else
                btn.Content = "X";



            totalclick++;


            btn.IsEnabled = false;   //當點過後就不能再按

            btnPriority.IsEnabled = false; //假如已經下了就不能改變先後手
            btnNewgame.IsEnabled = true; //有下過之後 才可以按重新開始
            //whichplayer = !whichplayer; // 交換符號

            if (totalclick < 10)//總點擊次數小於10才呼叫 AI
            {
                whoWin();

                if (checkwithComputer)//確認是否和電腦玩
                {
                    if (checkwinshow)  //假如有結果就不需要再呼叫 AI
                        callAI();
                }
                else                 //和朋友玩
                {
                    whichplayer = !whichplayer;//交換符號
                }

            }


            //whichplayer = !whichplayer;
        }
        private void callAI()
        {
            Random random = new Random();
            checkAIclick = true; //要假設 AI 還沒下
            

                whichplayer = !whichplayer;

            if (totalclick < 9)
                while (checkAIclick) //跑回圈直到成功下
                {
                    int a = random.Next(9) + 1;
                    switch (a)
                    {
                        case 1:
                            if (btn1.IsEnabled)
                            {
                                btn1.Content = "X";

                                btn1.IsEnabled = false;
                                checkAIclick = !checkAIclick;  //假如AI下了就要改變布林值 停止迴圈
                            }
                            break;
                        case 2:
                            if (btn2.IsEnabled)
                            {
                                btn2.Content = "X";
                                //whichplayer = !whichplayer;
                                btn2.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 3:
                            if (btn3.IsEnabled)
                            {
                                btn3.Content = "X";
                                //whichplayer = !whichplayer;
                                btn3.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 4:
                            if (btn4.IsEnabled)
                            {
                                btn4.Content = "X";
                                //whichplayer = !whichplayer;
                                btn4.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 5:
                            if (btn5.IsEnabled)
                            {
                                btn5.Content = "X";
                                //whichplayer = !whichplayer;
                                btn5.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 6:
                            if (btn6.IsEnabled)
                            {
                                btn6.Content = "X";
                                //whichplayer = !whichplayer;
                                btn6.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 7:
                            if (btn7.IsEnabled)
                            {
                                btn7.Content = "X";
                                //whichplayer = !whichplayer;
                                btn7.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 8:
                            if (btn8.IsEnabled)
                            {
                                btn8.Content = "X";
                                //whichplayer = !whichplayer;
                                btn8.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        case 9:
                            if (btn9.IsEnabled)
                            {
                                btn9.Content = "X";
                                //whichplayer = !whichplayer;
                                btn9.IsEnabled = false;
                                checkAIclick = !checkAIclick;
                            }
                            break;
                        default: break;
                    }
                }
            totalclick++;

            if (totalclick > 2)
            {
                whoWin();
            }

                whichplayer = !whichplayer;  //交換符號
            
        }

        private void whoWin()
        {

            bool winnercheck = false;  //判斷有沒有贏家

            //行判斷
            if ((btn1.Content == btn4.Content) && (btn1.Content == btn7.Content) && !btn1.IsEnabled)
            {
                //因為按鈕一開始content沒有值 因此要加上!btn1.IsEnabled 防止按一次就跳出遊戲結果  
                //btn1.Background = Brushes.Red; btn4.Background = Brushes.Red; btn7.Background = Brushes.Red;
                winnercheck = true;
            }
            else if ((btn2.Content == btn5.Content) && (btn2.Content == btn8.Content) && !btn2.IsEnabled)
            {
                //btn2.Background = Brushes.Red; btn5.Background = Brushes.Red; btn8.Background = Brushes.Red;
                winnercheck = true;
            }
            else if ((btn3.Content == btn6.Content) && (btn3.Content == btn9.Content) && !btn3.IsEnabled)
            {
                //btn3.Background = Brushes.Red; btn6.Background = Brushes.Red; btn9.Background = Brushes.Red;
                winnercheck = true;
            }
            //列判斷
            else if ((btn1.Content == btn2.Content) && (btn1.Content == btn3.Content) && !btn1.IsEnabled)
            {
                //btn1.Background = Brushes.Red; btn2.Background = Brushes.Red; btn3.Background = Brushes.Red;
                winnercheck = true;
            }
            else if ((btn4.Content == btn5.Content) && (btn4.Content == btn6.Content) && !btn4.IsEnabled)
            {
                //btn4.Background = Brushes.Red; btn5.Background = Brushes.Red; btn6.Background = Brushes.Red;
                winnercheck = true;
            }
            else if ((btn7.Content == btn8.Content) && (btn7.Content == btn9.Content) && !btn7.IsEnabled)
            {
                //btn7.Background = Brushes.Red; btn8.Background = Brushes.Red; btn9.Background = Brushes.Red;
                winnercheck = true;
            }
            //斜角判斷
            else if ((btn1.Content == btn5.Content) && (btn1.Content == btn9.Content) && !btn1.IsEnabled)
            {
                //btn1.Background = Brushes.Red; btn5.Background = Brushes.Red; btn9.Background = Brushes.Red;
                winnercheck = true;
            }
            else if ((btn3.Content == btn5.Content) && (btn3.Content == btn7.Content) && !btn3.IsEnabled)
            {
                //btn3.Background = Brushes.Red; btn5.Background = Brushes.Red; btn7.Background = Brushes.Red;
                winnercheck = true;
            }


            if (winnercheck)
            {

                endGame();

                String Winner;
                //結果彈跳視窗由使用者觸發 因此要反過來寫 找到遊戲結束時的使用者是誰
                if (whichplayer)
                {
                   
                    countO++; //O獲勝次數
                    labOcount.Content = countO;
                    Winner = "O";
                    if (checkwithComputer)
                        Winner = "玩 家";
                }
                else
                {
                    countX++; //X獲勝次數
                    labXcount.Content = countX;
                    Winner = "X";
                    if (checkwithComputer)
                        Winner = "電 腦";

                }
                if (checkwinshow)     //防止重複跳出視窗
                {


                    MessageBox.Show(Winner + " 贏了這場遊戲!", "遊戲結果");
                    checkwinshow = false;
                }

            }
            else
            {

                if (totalclick == 9)
                {
                    countTie++;
                    labcount.Content = countTie;
                    MessageBox.Show("和局", "遊戲結果");

                }
            }

        }

        
        private void endGame()
        {


            btn1.IsEnabled = false;
            btn2.IsEnabled = false;
            btn3.IsEnabled = false;
            btn4.IsEnabled = false;
            btn5.IsEnabled = false;
            btn6.IsEnabled = false;
            btn7.IsEnabled = false;
            btn8.IsEnabled = false;
            btn9.IsEnabled = false;


        }
        //遊戲開始
        private void startGame()
        {


            btn1.IsEnabled = true;
            btn2.IsEnabled = true;
            btn3.IsEnabled = true;
            btn4.IsEnabled = true;
            btn5.IsEnabled = true;
            btn6.IsEnabled = true;
            btn7.IsEnabled = true;
            btn8.IsEnabled = true;
            btn9.IsEnabled = true;


        }

        //重置紀錄
        private void btnResetrecord_Click(object sender, RoutedEventArgs e)
        {
            countO = 0;
            countTie = 0;
            countX = 0;
            labXcount.Content = countX;
            labcount.Content = countTie;
            labOcount.Content = countO;

        }
        //重新開始遊戲
        private void btnNewgame_Click(object sender, RoutedEventArgs e)
        {
            whichplayer = true;
            totalclick = 0;
            checkwinshow = true;

            btn1.Content = "";
            btn2.Content = "";
            btn3.Content = "";
            btn4.Content = "";
            btn5.Content = "";
            btn6.Content = "";
            btn7.Content = "";
            btn8.Content = "";
            btn9.Content = "";

            btn1.IsEnabled = true;
            btn2.IsEnabled = true;
            btn3.IsEnabled = true;
            btn4.IsEnabled = true;
            btn5.IsEnabled = true;
            btn6.IsEnabled = true;
            btn7.IsEnabled = true;
            btn8.IsEnabled = true;
            btn9.IsEnabled = true;
            
            if (ComputerFirst)//電腦先手就會成立並呼叫AI
                callAI();
        }
        //和電腦玩
        private void btnwithComputer_Click(object sender, RoutedEventArgs e)
        {
            labelMode.Content = "目 前 模 式 為 和 電 腦 玩 ( 玩 家 先 手 )";
            checkwithComputer = true;
            btnPriority.IsEnabled = true;  //按下和電腦玩才可以選擇先下後下
            btnwithComputer.IsEnabled = false;
            btnwithFriends.IsEnabled = false;
            startGame();
        }
        //決定電腦先下後下
        private void btnPriority_Click(object sender, RoutedEventArgs e)
        {
            labelMode.Content = "目 前 模 式 為 和 電 腦 玩 ( 電 腦 先 手 )";
            ComputerFirst = true;
            callAI();//電腦先下所以呼叫 AI
            btnPriority.IsEnabled = false;
            
        }
        //和朋友玩
        private void btnwithFriends_Click(object sender, RoutedEventArgs e)
        {
            labelMode.Content = "目 前 模 式 為 和 朋 友 玩";
            btnwithComputer.IsEnabled = false;
            btnPriority.IsEnabled = false;
            btnwithFriends.IsEnabled = false;
            startGame();
        }
        //重新開始遊戲 並選取遊戲模式
        private void btnChangeMode_Click(object sender, RoutedEventArgs e)
        {
            labelMode.Content = "請 先 選 擇 模 式";
            btnNewgame.IsEnabled = false;
            whichplayer = true;
            totalclick = 0;
            checkwinshow = true;
            checkwithComputer = false;
            ComputerFirst = false;

            btn1.Content = "";
            btn2.Content = "";
            btn3.Content = "";
            btn4.Content = "";
            btn5.Content = "";
            btn6.Content = "";
            btn7.Content = "";
            btn8.Content = "";
            btn9.Content = "";

            endGame();

            btnwithComputer.IsEnabled = true;
            btnwithFriends.IsEnabled = true;
            btnPriority.IsEnabled = false;
        }

        private void mouseEnter(object sender, MouseEventArgs e)
        {
            Button btn = (Button)sender;


            if (whichplayer)
                btn.Content = "O";
            else
                btn.Content = "X";

        }

        private void mouseLeave(object sender, MouseEventArgs e)
        {
            Button btn = (Button)sender;
            //按鈕可按時才需要變化 不然離開就會把CONTENT清除
            if (btn.IsEnabled)
                btn.Content = "";

        }
    }
}
