# [Tic-Tac-Toe](https://drive.google.com/file/d/0B8IEoTTY-lg7ODAySVpRSDdsdzg/view?usp=sharing)

## 遊戲畫面

<img src="https://gitlab.com/rayox349/kinnect/raw/master/WpfApplication1-Tic-Tac-Toe/IMG/ooxx.png" width = "800" height = "370" alt="" align=center /> 

# [Ordering system](https://drive.google.com/file/d/0B0qx0jtSmUDQUFE3X2pBbGgzaGc/preview)

## 切換餐點

<img src="https://gitlab.com/rayox349/kinnect/raw/master/WpfApplication1-Ordering%20system/GIF/switch.gif" width = "400" height = "300" alt="" align=center />

## 確認餐點

<img src="https://gitlab.com/rayox349/kinnect/raw/master/WpfApplication1-Ordering%20system/GIF/check.gif" width = "400" height = "300" alt="" align=center />

## 結束點餐

<img src="https://gitlab.com/rayox349/kinnect/raw/master/WpfApplication1-Ordering%20system/GIF/finish.gif" width = "400" height = "300" alt="" align=center />